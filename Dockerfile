FROM node:14.9-alpine as build

WORKDIR /usr/share/tariff-svc

ADD . ./

RUN apk add --no-cache make g++ python \
  && npm install -g @nestjs/cli &&  npm install --production

RUN npm run build

FROM node:14.9-alpine

RUN apk add --no-cache libpq

ADD https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/v0.3.2/grpc_health_probe-linux-amd64 /bin/grpc_health_probe

RUN chmod +x /bin/grpc_health_probe

WORKDIR /usr/share/tariff-svc

COPY --from=build /usr/share/tariff-svc/node_modules ./node_modules
COPY --from=build /usr/share/tariff-svc/dist ./dist

EXPOSE 50051

CMD ["node", "dist/main.js"]
